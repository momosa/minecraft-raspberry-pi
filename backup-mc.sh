#!/bin/sh -e

TARGET_WITH_LINKS="${HOME}/minecraft-server"
TARGET="$(readlink --canonicalize "${TARGET_WITH_LINKS}")"
TARGET_PARENT="$(dirname ${TARGET})"
TARGET_NAME="$(basename "${TARGET}")"

# Date/time example: 2022-09-19T1758
DATE_TIME=$(date --utc "+%Y-%m-%dT%H%M")
START_DIR="${PWD}"

BACKUP_DIR="${HOME}/backups"
BACKUP_FILE="${BACKUP_DIR}/${TARGET_NAME}-${DATE_TIME}.tar.gz"

validate() {
    if [ -f "${BACKUP_FILE}" ]; then
        echo "ERROR: Backup file '${BACKUP_FILE}' already exists" 1>&2
        exit 1
    fi
    if [ ! -e "${TARGET}" ]; then
        echo "ERROR: Backup target '${TARGET}' does not exist" 1>&2
        echo "ERROR: Bad backup target derived from '${TARGET_WITH_LINKS}'" 1>&2
        exit 1
    fi
}

create_backup() {
    if [ ! -d "${BACKUP_DIR}" ]; then 
        echo "Creating directory ${BACKUP_DIR}"
        mkdir -p "${BACKUP_DIR}"
    fi
    echo "Backup target: '${TARGET}'"
    cd "${TARGET_PARENT}"
    echo "Creating backup file '${BACKUP_FILE}'..."
    tar -czf "${BACKUP_FILE}" "${TARGET_NAME}"
    cd "${START_DIR}"
}


validate
create_backup
