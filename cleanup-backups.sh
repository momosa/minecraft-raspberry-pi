#!/bin/sh -e

MAX_DAYS_OLD=7
MAX_BACKUPS=12
BACKUP_DIR="${HOME}/backups"

validate() {
    if [ ! -d "${BACKUP_DIR}" ]; then 
        echo "ERROR: The backup directory ${BACKUP_DIR} does not exist" 1>&2
        exit 1
    fi

    N_BACKUPS=$(find "${BACKUP_DIR}" -type f | wc -l)
    N_OLD_BACKUPS=$(find "${BACKUP_DIR}" -type f -mtime +${MAX_DAYS_OLD} | wc -l)
    if [ ${N_BACKUPS} -eq 1 ]; then
        echo "WARNING: There is/are only ${N_BACKUPS} backup/s: not enough to clean up." 1>&2
        exit 0
    fi
}

delete_all_but_newest() {
    # WARNING: not recursive
    n_newest=$1
    echo "Deleting backups excluding ${n_newest} newest backups ..."
    START_DIR="${PWD}"
    cd "${BACKUP_DIR}"
    ls -1ltr --color=never | head -n -${n_newest} | cut -d' ' -f9 | xargs -d '\n' rm -f --
    cd "${START_DIR}"
}

cleanup() {

    delete_all_but_newest ${MAX_BACKUPS}

    N_BACKUPS=$(find "${BACKUP_DIR}" -type f | wc -l)
    N_OLD_BACKUPS=$(find "${BACKUP_DIR}" -type f -mtime +${MAX_DAYS_OLD} | wc -l)

    if [ ${N_BACKUPS} -eq ${N_OLD_BACKUPS} ]; then
        echo "All backups are older than ${MAX_DAYS_OLD} days"
        delete_all_but_newest 1
    else
        echo "Deleting backups older than ${MAX_DAYS_OLD} days ..."
        find "${BACKUP_DIR}" -type f -mtime +${MAX_DAYS_OLD} -delete
    fi
}

validate
cleanup
