autocmd BufNewFile,BufRead *.sh
\ setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=100 expandtab autoindent fileformat=unix

autocmd BufNewFile,BufRead *.json
\ setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=100 expandtab autoindent fileformat=unix

autocmd BufNewFile,BufRead *.yaml,.*yml
\ setlocal tabstop=2 softtabstop=2 shiftwidth=2 textwidth=100 expandtab autoindent fileformat=unix

" Ctrl+c copy to clipboard
vnoremap <C-c> "+y

" Folding
set foldcolumn=2
set foldlevel=0
set foldmethod=indent
set nofoldenable

" This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>

"  force  a :redraw! on "events" that scramble the screen with
"  syntax highlighting enabled.
set ttyfast
au FileWritePost * :redraw!
au TermResponse * :redraw!
au TextChanged * :redraw!
au QuickFixCmdPre * :redraw!
au QuickFixCmdPost * :redraw!
