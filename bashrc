#############################
# Append the following to the bashrc

alias ls="ls -lh --color"
alias cp="rsync --archive --human-readable"

# To see 256 colors:
# for (( i=0; i<256; i++ )); do echo -e "\033[38;5;${i}mColor ${i}\033[0m"; done
# 256 color format: \033[38;5;${COLOR_NUM}m
# The following uses color 215:
PS1="$(cat << COMMAND_PROMPT
\[\033[38;5;215m\]================================================================================
\w
\t
> \[\033[0m\]
COMMAND_PROMPT
)"

export EDITOR=vim

# vim in bash prompt
set -o vi in .bashrc
