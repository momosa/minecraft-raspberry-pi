# Raspberry Pi Minecraft server

- - -

## Contents

1. [Raspberry Pi setup](#raspberry_pi_setup)

    1.1. [Installing the Ubuntu server](#installing_ubuntu_server)

    1.2. [Setting up the Ubuntu server OS](#setting_up_ubuntu_server)

2. [Setting up the Minecraft server](#setting_up_minecraft_server)

    2.1. [Installing the Minecraft server](#installing_minecraft_server)

    2.2. [Minecraft server settings](#minecraft_server_settings)

    2.3. [Running the Minecraft server](#running_minecraft_server)

3. [Accessing the Minecraft server](#accessing_the_minecraft_server)
4. [Server admin](#server_admin)

    4.1. [Daemonizing the Minecraft server](#daemonization)

    4.2. [Backing up the server](#backups)

    4.3. [Storing server logs](#logs)

    4.4. [Performance analysis](#performance)

5. [Server commands](#server_commands)

- - -

<a name="raspberry_pi_setup"></a>

## 1\. Raspberry Pi setup for Raspberry Pi 4

<a name="installing_ubuntu_server"></a>

### 1.1\. Installing the Ubuntu server 22.04 64-bit OS

The Raspberry Pi uses a micro-SD as a hard drive.
You can set up the micro-SD on another computer.

See [this short video][install_ubuntu_server_on_raspberry_pi_4].
There are plenty of others online, too.

Setup instructions for Ubuntu 20.04.

Use the USB adapter to connect the microSD to your computer.
I tried using an SD card adapter,
but it was slow and the update ultimately failed.

Install or update the imager:

```shell
# Install the imager
sudo snap install rpi-imager

# Update the imager
sudo snap refresh rpi-imager
```

Open the imager.

Select the operating system:
Ubuntu Server 22.04 64-bit

Select the media (microSD)

Leave settings alone unless you know what you're doing.

Write to the media.

Plug the microSD into the Raspberry Pi.

Turn on the Raspberry Pi.

<a name="setting_up_ubuntu_server"></a>

### 1.2\. Setting up the Ubuntu Server OS

What we need:

1. Access the server.
1. Update the server.
1. Enable secure `ssh` access.
1. Customize the experience.

#### 1.2.1\. Access the server

To connect to the internet, either

1. just use ethernet, or
1. enable wifi access by following the instructions in [this short video][install_ubuntu_server_on_raspberry_pi_4].

Next, we need to find the IP. Options:

1. Connect a keyboard and monitor to the Raspberry Pi,
   run `hostname -I`,
   and find the IP on the local network (`192.168.blah.blah`).
2. Log into your router,
   view connected devices,
   and find the Raspberry Pi.
   Mine was labeled `ubuntu`.

Now, you can log into the Pi from other devices on the network.
The default username/password is `ubuntu`/`ubuntu`,
although you'll be asked to change the password on your first login.
I recommend using a password manager to create a secure password.

#### 1.2.2\. Update the server

```shell
ssh ubuntu@192.168.blah.blah
```

```shell
sudo apt update
sudo apt upgrade -y
sudo apt autoremove
sudo apt install -y openjdk-17-jre-headless net-tools
```

Open the port used by Minecraft.
This is required for people to log onto your MC server
once it's running.

```shell
sudo ufw allow 25565
```

You may need to restart at this point.
Restarting will end your session,
so give it a minute and log back in if you have more to do.

```shell
sudo shutdown -r now
```

#### 1.2.3\. Enable secure `ssh` access

From your local machine:

```shell
# Generate the key pair
ssh-keygen -f ~/.ssh/minecraft-raspberry-pi -t rsa -b 4096

# Place the key on the host
ssh-copy-id -i ~/.ssh/minecraft-raspberry-pi ubuntu@192.168.blah.blah
# You'll need to enter the password you use to log in to the Pi.
```

You can now log in to the Pi with:

```shell
# Note: this is an intermediate way to log in
ssh -i ~/.ssh/minecraft-raspberry-pi ubuntu@192.168.blah.blah
```

Let's make that a little cleaner.
Add the following entry to the `~/.ssh/config`
of your local machine:

```
Host minecraft-raspberry-pi
    HostName 192.168.blah.blah
    User ubuntu
    IdentityFile ~/.ssh/minecraft-raspberry-pi
```

You can now log in to the Pi with:

```shell
ssh minecraft-raspberry-pi
```

The preceding will also enable you to
copy files onto the server easily
using `rsync` or `scp`.

#### 1.2.4\. Customize the shell

This repo contains a `bashrc` file
with a couple of starter customizations.
Just append them to `minecraft-raspberry-pi:~/.bashrc`
and your next session will look much better.

- - -

<a name="setting_up_minecraft_server"></a>

## 2\. Setting up the Minecraft server

This section is about
getting the server up and running on the Pi.

<a name="installing_minecraft_server"></a>

### 2.1\. Installing the Minecraft server

Here are instruction for installing the server and
(optionally) restoring a backup.

Unfortunately, Curseforge makes it a pain to use `wget` from the Pi.
So we'll set up the basics on our local machine,
then move things to the Pi.

1. Find the _SERVER_ version your modpack on Curseforge or elsewhere.
   It should say "server".
   If it's unlabeled, it's probably the client.
1. Decompress the server files.
1. Place the backup files with the server files.
1. Use `rsync` to move everything to the Pi.

Suppose you decompress the server files into the directory:

```shell
~/minecraft-server
```

Then place the backup files there,
overwriting existing files as needed.

Use the following command to
copy `~/minecraft-server` into
the home directory of the Pi:

```shell
rsync \
    --archive \
    --human-readable \
    --compress \
    --verbose \
    ~/minecraft-server \
    minecraft-raspberry-pi:~/
```

<a name="minecraft_server_settings"></a>

### 2.2\. Minecraft server Settings

This is a good time to adjust your Minecraft server settings.

Your basic MC settings are in `minecraft-server/server.properties`.
Many mods will have their own config files or directories.
Have a look around and adjust as you see fit.

#### 2.2.1\. Managing performance

`ServerStart.sh` has parameters for how the server is run,
like how much memory to allocate.
If you don't have that script, here is a sample java call
that starts a server:

```shell
java -jar \
    -Xms6G \
    -Xmx6G \
    -XX:+UseG1GC \
    -XX:+UnlockExperimentalVMOptions \
    -XX:MaxGCPauseMillis=100 \
    -XX:+DisableExplicitGC \
    -XX:TargetSurvivorRatio=90 \
    -XX:G1NewSizePercent=50 \
    -XX:G1MaxNewSizePercent=80 \
    -XX:G1MixedGCLiveThresholdPercent=50 \
    -XX:+AlwaysPreTouch \
    ./minecraft.jar nogui
```

Some modpacks use a lot of resources.
in that event, consider modifying these items
in `server.properties`.
Check out [the documentation][server_properties] for details.

```
max-tick-time=-1
```

The `max-tick-time` designates
the maximum duration of a game tick
before the game crashes.
The value is measured in milliseconds, e.g., 60000 is 1min.
Consider setthing the value to 300000 (5min)
or -1, in which case the game will not crash
no matter how long it takes to process a tick.

Note that increasing `max-tick-time`
avoids crashes due to processing spikes,
but doesn't improve overall performance.

```
view-distance=8
```

`view-distance` is the number of chunks away the player can see.
Decreasing the `view-distance` to 8 or lower can improve performance.

```
simulation-distance=8
```

`simulation-distance` is the number of chunks away
the server calculates ticks and spawning.
Decreasing the `simulation-distance` can improve performance.

<a name="running_minecraft_server"></a>

### 2.3\. Running the Minecraft server

If we just run the server,
as with `ServerStart.sh`,
the server will be attached to your session.
We want to be able to keep the MC server running
independently of our session,
even when we're not logged into the Pi.

Two good options are:

1. run the Minecraft server with a terminal multiplexer like `screen`, or
1. run the Minecraft server as a daemon.

This section covers the first approach.
See [Daemonizing the Minecraft server](#daemonization) for the second approach.

TL;DR: the folling will run the MC server in an isolated session.

```shell
# Attach to a session for the server
screen -DR minecraft-server

# Start the server
# If you have permission issues, chmod 755 ServerStart.sh
./ServerStart.sh

# Detach from the session:
# Ctrl-a d
```

You should build up a little `screen` cheatsheet for yourself.
Here are a few basics:

```
# The man page is quite helpful
man screen

# Attach here and how
# This will attach to an existing session if one exists,
# or create a session by that name if it doesn't
screen -DR foo

# Create a session named foo and attach
screen -S foo

# List existing sessions
screen -ls

# Attach to a detached session
screen -r 

# Attach to an attached session, detaching it first
screen -dr 

# Detach from a session, but don't end it
Ctrl-a d

# End the current session
Ctrl-d
```

- - -

<a name="accessing_minecraft_server"></a>

## 3\. Accessing the Minecraft server from the client

1. Expose the port on the Pi.
2. Expose the port on the router.

We already exposed the MC port on the Pi.
See [Setting up the Ubuntu server OS](#setting_up_ubuntu_server).

Next, we need to expose the MC port on the router.
See [the Linksys docs on single port forwarding][single_port_forwarding_linksys].
Log into your router and
Create a new rule for `192.168.blah.blah`, port 25565.

Once the port is accessible and the server is running,
you should be able to log in from your MC client.

From your local network, you can add a server with address:

```
192.168.blah.blah:25565
```

To access the server from outside of your local network,
use the public IP instead of the local one.
To get the public IP, run this command from the Pi:

```shell
# Get the public IP address
wget -O - -q https://checkip.amazonaws.com
```

- - -

<a name="server_admin"></a>

## 4\. Server admin

This section is for people who want to address questions like:

- What if the server crashes?
- What if we want backups?
- What if we want logs?
- How do we manage all of that data?

<a name="daemonization"></a>

### 4.1\. Daemonizing the server

We'll use `systemd` to manage the server as a daemon.
This will give us a common interface for interacting with the server,
plus a lot of great perks like auto-restarts.

Place the MC server into `~/minecraft-server`
Alternatively, create a soft link from `~/minecraft-server` to the directory of the server.
The second option will make it easy to switch server directories.

```shell
# from ~/
ln -s modpack-name-1.2.3 minecraft-server
```

Place the `minecraft-server.service` file from this repo
into `/etc/systemd/system/` on the Pi.
You may need root permissions,
so just use sudo to open the target file and paste the contents:

```shell
sudo vim /etc/systemd/system/minecraft-server.service
```

Enable the service with the following.
This requires a password.

```shell
systemctl enable minecraft-server
```

You can do a lot using `systemctl` to manage the server.

```shell
systemctl -h
```

A few basics:

```shell
systemctl start minecraft-server
systemctl stop minecraft-server
```

Note: So far, we've been using 

<a name="backups"></a>

### 4.2\. Backing up the server

#### 4.2.1\. Manual backups

You can create a manual backup by
executing the `backup-mc.sh` script on the server.
Note that the script assumes
your minecraft server directory is `~/minecraft-server`.
The backup will be placed in `~/backups/` on the server.

To start, copy the script onto the server.
If your `~/.ssh/config` is setup as [described above](#setting_up_ubuntu_server),
you can use:

```shell
rsync backup-mc.sh minecraft-raspberry-pi:~/
```

Next, hop onto the server:

```shell
ssh minecraft-raspberry-pi
```

Stop the minecraft server before creating the backup.

Finally, create the backup.
The script may take a few minutes to execute.
Consider running it in a `screen` if
you're backing up a lot of data.

```shell
./backup-mc.sh
```

Your backup will be located in `~/backups/` on the server.

```shell
ls -lh ~/backups
```

Storing too many backups may fill up your hard drive.
Be sure to remove old backups.
See the section below on cleaning up.

#### 4.2.2\. Restoring a backup

Create a temporary directory to avoid name conflicts.

```shell
[ -d ~/tmp ] || mkdir ~/tmp
```

Unpack your desired backup into `~/tmp/`:

```shell
tar -xzvf ~/backups/server-backup.tar.gz --directory ~/tmp
```

#### 4.2.3\. Manual backup cleanup

This repo comes with a cleanup script, `cleanup-backups.sh`.
The cleanup script will:

1. Leave the newest backup, if there is one, regardless of how old it is.
1. Keep no more than a certain number of backups.
1. Remove backups older than a certain number of days.

You can modify the script to view and change its parameters.

To use the script, place it onto the server:

```shell
rsync cleanup-backups.sh minecraft-raspberry-pi:~/
```

Next, hop onto the server:

```shell
ssh minecraft-raspberry-pi
```

Finally, run the script:

```shell
./cleanup-backups.sh
```

<a name="logs"></a>

### 4.3\. Storing server logs

TODO

<a name="performance"></a>

### 4.4\. Performance analysis

TODO

- - -

<a name="server_commands"></a>

## 5\. Server commands

See [the console commands wiki][console_commands].

Set min percentage of players sleeping
necessary to sleep through the night.

```
/gamerule playersSleepingPercentage 50
```

[install_ubuntu_server_on_raspberry_pi_4]: https://www.youtube.com/watch?v=VVtdGczo3hM
[single_port_forwarding_linksys]: https://www.linksys.com/us/support-article?articleNum=138535
[console_commands]: https://minecraft.fandom.com/wiki/Commands
[server_properties]: https://minecraft.fandom.com/wiki/Server.properties
